# OUHK Schedule iCal Generator for Python 3

### Install requirements.
```
pip install selenium icalendar
```

### Add your student id(sXXXXXXX) and password to exec.py.
```
ouhk_username="<student id>"
ouhk_password="<password>"
```

### Execute exec.py.
```
python exec.py
```
