import json
import pytz
import re
import time
import uuid
from datetime import datetime, timedelta, date
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from icalendar import Calendar, Event, Alarm

######################### Create iCalendar ##########################
HK = pytz.timezone('Asia/Hong_Kong')

cal = Calendar()
cal.add('prodid', '-//hamayi//OUHK//')
cal.add('version', '2.0')

ouhk_username=""
ouhk_password=""

options = webdriver.ChromeOptions()
options.add_argument('--headless')
driver = webdriver.Chrome('./chromedriver.exe', options = options)  # Optional argument, if not specified will search path.

######################### Download Course Data ##########################
driver.get('https://schedule.ouhk.edu.hk/');
#time.sleep(3) # Let the user actually see something!
input_login_user = driver.find_element_by_name("loginid")
input_login_pass = driver.find_element_by_name("password")
input_login_btn = driver.find_element_by_xpath("/html/body/form/center/table[2]/tbody/tr/td[1]/table/tbody/tr/td[2]/div/a")

input_login_user.send_keys(ouhk_username)
input_login_pass.send_keys(ouhk_password)
input_login_btn.send_keys(Keys.ENTER)

# Nav to left sidebar
leftSidebar = driver.find_element_by_name("leftFrame")
driver.get(leftSidebar.get_attribute("src"))

courseList = driver.find_element_by_id("courseList")

Assignments = []
Tutorial = []
Surgery = []

# Get Course Schedule URL
for courseItem in courseList.find_elements_by_tag_name("li"):
	timeTable = courseItem.find_element_by_tag_name("a")
	Assignments.append(re.findall(".*schTypeName=Assignments", timeTable.get_attribute("href")))
	Tutorial.append(re.findall(".*schTypeName=Tutorial", timeTable.get_attribute("href")))
	Surgery.append(re.findall(".*schTypeName=Surgery", timeTable.get_attribute("href")))

# Remove empty result of the list
Assignments = [x for x in Assignments if x != []]
Tutorial = [x for x in Tutorial if x != []]
Surgery = [x for x in Surgery if x != []]

######################### Assignments ##########################
AssignmentsList = []
for item in Assignments:
	#print(item)
	for url in item:
		driver.get(url)

		title = driver.find_element_by_xpath("/html/body/form/table[1]/tbody/tr/td[2]").text.strip().split()
		tbody = driver.find_element_by_xpath("/html/body/form/table[2]/tbody")

		AssignmentsTable = []
		for tr in tbody.find_elements_by_tag_name("tr"):
			AssignmentsDetails = []
			for td in tr.find_elements_by_tag_name("td"):
				AssignmentsDetails.append(td.text.strip())

			fullDate = AssignmentsDetails[1].split()
			newDate = fullDate[2] + " " + fullDate[1] + " " + fullDate[0]

			result = datetime.strptime(newDate, '%Y %b %d').date()
			dueDate = str(result).split("-")
			dueDate2 = str(result + timedelta(days=1)).split("-")

			event = Event()
			event.add('summary', "End of " + AssignmentsDetails[2] + " - " + title[0])
			event.add('dtstart', date(int(dueDate[0]),int(dueDate[1]),int(dueDate[2])))
			event.add('dtend', date(int(dueDate2[0]),int(dueDate2[1]),int(dueDate2[2])))
			event.add('dtstamp', datetime(int(dueDate[0]),int(dueDate[1]),int(dueDate[2])))
			event['uid'] = uuid.uuid4()

			alarm1 = Alarm()
			alarm1.add(name='action', value='DISPLAY')
			alarm1.add(name='trigger', value=timedelta(days=-1))
			alarm1.add(name='description', value='Deadline reminder 1')

			alarm2 = Alarm()
			alarm2.add(name='action', value='DISPLAY')
			alarm2.add(name='trigger', value=timedelta(weeks=-1))
			alarm2.add(name='description', value='Deadline reminder 2')

			event.add_component(alarm1)
			event.add_component(alarm2)

			cal.add_component(event)

########################## Tutorial ##########################
TutorialList = []
for item in Tutorial:
	for url in item:
		driver.get(url)

		title = driver.find_element_by_xpath("/html/body/form/table[1]/tbody/tr/td[2]").text.strip().split()
		tbody = driver.find_element_by_xpath("/html/body/form/table[2]/tbody")

		TutorialTable = []
		for tr in tbody.find_elements_by_tag_name("tr"):
			TutorialDetails = []
			for td in tr.find_elements_by_tag_name("td"):
				TutorialDetails.append(td.text.strip())

			fullDate = TutorialDetails[2].split()
			newDate = fullDate[2] + " " + fullDate[1] + " " + fullDate[0]

			fullTime = TutorialDetails[3].split("-")
			startTime = fullTime[0].split(":")
			endTime = fullTime[1].split(":")

			startHour = startTime[0].strip()
			startMinute = startTime[1].strip()

			endHour = endTime[0].strip()
			endMinute = endTime[1].strip()

			result = datetime.strptime(newDate, '%Y %b %d').date()
			dueDate = str(result).split("-")

			event = Event()
			event.add('summary', "[" + TutorialDetails[0] + "] Tutorial #" + TutorialDetails[1] + " | " + title[0])
			event.add('dtstart', datetime(int(dueDate[0]),int(dueDate[1]),int(dueDate[2]),int(startHour),int(startMinute),0, tzinfo=HK))
			event.add('dtend', datetime(int(dueDate[0]),int(dueDate[1]),int(dueDate[2]),int(endHour),int(endMinute),0, tzinfo=HK))
			event.add('dtstamp', datetime(int(dueDate[0]),int(dueDate[1]),int(dueDate[2]),int(startHour),int(startMinute),0, tzinfo=HK))
			event.add('location', TutorialDetails[4] + " " + TutorialDetails[5])
			event['uid'] = uuid.uuid4()
			

			alarm1 = Alarm()
			alarm1.add(name='action', value='DISPLAY')
			alarm1.add(name='trigger', value=timedelta(days=-1))
			alarm1.add(name='description', value='Deadline reminder 1')

			alarm2 = Alarm()
			alarm2.add(name='action', value='DISPLAY')
			alarm2.add(name='trigger', value=timedelta(hours=-2))
			alarm2.add(name='description', value='Deadline reminder 2')

			event.add_component(alarm1)
			event.add_component(alarm2)

			cal.add_component(event)

########################## Surgery ##########################
SurgeryList = []
for item in Surgery:
	for url in item:
		driver.get(url)

		title = driver.find_element_by_xpath("/html/body/form/table[1]/tbody/tr/td[2]").text.strip().split()
		tbody = driver.find_element_by_xpath("/html/body/form/table[2]/tbody")

		SurgeryTable = []
		for tr in tbody.find_elements_by_tag_name("tr"):
			SurgeryDetails = []
			for td in tr.find_elements_by_tag_name("td"):
				SurgeryDetails.append(td.text.strip())

			fullDate = SurgeryDetails[2].split()
			newDate = fullDate[2] + " " + fullDate[1] + " " + fullDate[0]

			fullTime = SurgeryDetails[3].split("-")
			startTime = fullTime[0].split(":")
			endTime = fullTime[1].split(":")

			startHour = startTime[0].strip()
			startMinute = startTime[1].strip()

			endHour = endTime[0].strip()
			endMinute = endTime[1].strip()

			result = datetime.strptime(newDate, '%Y %b %d').date()
			dueDate = str(result).split("-")

			event = Event()
			event.add('summary', "[" + SurgeryDetails[0] + "] Surgery #" + SurgeryDetails[1] + " | " + title[0])
			event.add('dtstart', datetime(int(dueDate[0]),int(dueDate[1]),int(dueDate[2]),int(startHour),int(startMinute),0, tzinfo=HK))
			event.add('dtend', datetime(int(dueDate[0]),int(dueDate[1]),int(dueDate[2]),int(endHour),int(endMinute),0, tzinfo=HK))
			event.add('dtstamp', datetime(int(dueDate[0]),int(dueDate[1]),int(dueDate[2]),int(startHour),int(startMinute),0, tzinfo=HK))
			event.add('location', SurgeryDetails[4] + " " + SurgeryDetails[5])
			event.add('transp', 'opaque')
			event['uid'] = uuid.uuid4()
			
			
			alarm1 = Alarm()
			alarm1.add(name='action', value='DISPLAY')
			alarm1.add(name='trigger', value=timedelta(days=-1))
			alarm1.add(name='description', value='Deadline reminder 1')

			alarm2 = Alarm()
			alarm2.add(name='action', value='DISPLAY')
			alarm2.add(name='trigger', value=timedelta(hours=-2))
			alarm2.add(name='description', value='Deadline reminder 2')

			event.add_component(alarm1)
			event.add_component(alarm2)

			cal.add_component(event)

		title = driver.find_element_by_xpath("/html/body/form/table[1]/tbody/tr/td[2]")
		tbody = driver.find_element_by_xpath("/html/body/form/table[2]/tbody")

# Kill webdriver
driver.quit()

f = open('ouhk.ics', 'wb')
f.write(cal.to_ical())
f.close()